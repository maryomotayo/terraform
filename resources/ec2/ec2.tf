

#################################ec2 instance###########################

resource "aws_instance" "web" {
  count                  = 2
  ami                    = var.ami
  instance_type          = var.ec2_instance_type
  iam_instance_profile   = aws_iam_instance_profile.ec2-profile.name
  key_name               = var.key_pair_name
  vpc_security_group_ids = [aws_security_group.sg.id]
  user_data              = data.template_file.userdata[count.index].rendered
  subnet_id              = data.terraform_remote_state.vpc.outputs.public_subnet_1_id

  tags = {
    Name        = "${var.webserver1_Name}${count.index + 1}"
    Environment = var.vpc_Environment
  }


  // lifecycle {
  //   ignore_changes = [user_data]
  // }
}


