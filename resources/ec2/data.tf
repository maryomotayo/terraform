data "template_file" "userdata" {
  template = "${file("${path.module}/userdata.sh")}"
  count    = 2
  vars = {
    log_name      = var.log_name
    WebserverName = "${var.webserver1_Name}${count.index + 1}"
  }
}


data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    region = "eu-west-1"
    key    = "interview/vpc_backend.tfstate"
    bucket = "devops-interview-bucket"
  }
}