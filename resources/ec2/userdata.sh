#!/bin/bash
sudo yum update -y
sudo yum install mysql
# sudo mysql -u root -h db_host -p
# sudo create database testdb;
sudo yum install httpd -y
sudo systemctl start httpd
sudo chkconfig httpd on
echo '<h1>This is Deep3 DevOps Interview ${WebserverName}</h1>' > /var/www/html/index.html
sudo yum install -y awslogs
sudo sed -i -e 's/us-east-1/eu-west-1/g' /etc/awslogs/awscli.conf
sudo sed -i -e 's|log_group_name = /var/log/messages|log_group_name = ${log_name}|g' /etc/awslogs/awslogs.conf
sudo systemctl start awslogsd
sudo systemctl enable awslogsd.service