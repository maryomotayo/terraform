
######## VPC CREATION ############

resource "aws_vpc" "deep3-vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name        = var.vpc_Name
    Environment = var.vpc_Environment
  }
}

######## PUBLIC SUBNET CREATION ###########


resource "aws_subnet" "public-subnet-1" {
  cidr_block              = var.public_subnet_1_cidr
  vpc_id                  = aws_vpc.deep3-vpc.id
  map_public_ip_on_launch = "true"
  availability_zone       = var.public_subnet1_availability_zone

  tags = {
    Name        = var.webserver-subnet1_Name
    Environment = var.vpc_Environment
  }
}

resource "aws_subnet" "public-subnet-2" {
  cidr_block              = var.public_subnet_2_cidr
  vpc_id                  = aws_vpc.deep3-vpc.id
  map_public_ip_on_launch = "true"
  availability_zone       = var.public_subnet2_availability_zone

  tags = {
    Name        = var.webserver-subnet2_Name
    Environment = var.vpc_Environment

  }
}
####### PRIVATE SUBNET CREATION #########

resource "aws_subnet" "private-subnet-1" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.deep3-vpc.id
  availability_zone = var.private_subnet1_availability_zone

  tags = {
    Name        = var.appserver-subnet1_Name
    Environment = var.vpc_Environment
  }
}

resource "aws_subnet" "private-subnet-2" {
  cidr_block        = var.private_subnet_2_cidr
  vpc_id            = aws_vpc.deep3-vpc.id
  availability_zone = var.private_subnet2_availability_zone

  tags = {
    Name        = var.appserver-subnet2_Name
    Environment = var.vpc_Environment
  }
}

######## PRIVATE ROUTE TABLE CREATION #######

resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.deep3-vpc.id

  tags = {
    Name        = var.private_RT_Name
    Environment = var.vpc_Environment
  }
}

########## PUBLIC ROUTE TABLE CREATION ########

resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.deep3-vpc.id

  tags = {
    Name        = var.public_RT_Name
    Environment = var.vpc_Environment
  }
}

######### APP PRIVATE SUBNET ASSOCIATION TO ROUTE TABLE##########

resource "aws_route_table_association" "private-subnet-1-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-1.id
}
resource "aws_route_table_association" "private-subnet-2-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-2.id
}

######### PUBLIC SUBNET ASSOCIATION TO ROUTE TABLE########

resource "aws_route_table_association" "public-subnet-1-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-1.id
}

resource "aws_route_table_association" "public-subnet-2-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-2.id
}

# An Elastic IP (eip) is needed for NAT-GATEWAY 
########## CREATE AN EIP ###############

resource "aws_eip" "elastic-ip-for-nat-gw" {
  vpc                       = true
  associate_with_private_ip = var.associate_with_private_ip

  tags = {
    Name = var.EIP_Name
  }
}

# AllOW PRIVATE SUBNET TO ACCESS THE INTERNET THROUGH NAT-GATEWAY
#########  CREATING NAT GATEWAY###############

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip-for-nat-gw.id
  subnet_id     = aws_subnet.public-subnet-1.id

  tags = {
    Name = var.nat-gw_Name
  }
  depends_on = [aws_eip.elastic-ip-for-nat-gw]
}

######### ASSOCIATE THE NAT-GW WITH PRIVATE ROUTE-TABLE############

resource "aws_route" "nat-gw-route" {
  route_table_id         = aws_route_table.private-route-table.id
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
  destination_cidr_block = var.destination_cidr_block
}

############ CREATING INTERNET GATEWAY ################
resource "aws_internet_gateway" "webserver-igw" {
  vpc_id = aws_vpc.deep3-vpc.id

  tags = {
    Name        = var.IGW_Name
    Environment = var.vpc_Environment
  }
}

######## ASSOCIATE THE PUBLIC-IGW TO ROUTE TABLE##########

resource "aws_route" "public-internet-gw-route" {
  route_table_id         = aws_route_table.public-route-table.id
  gateway_id             = aws_internet_gateway.webserver-igw.id
  destination_cidr_block = var.destination_cidr_block
}