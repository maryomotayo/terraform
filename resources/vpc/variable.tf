provider "aws" {
  region = var.region
}
terraform {
  backend "s3" {}
}

variable "region" {
  default     = "eu-west-1"
  description = "aws region to deploy infrastructure"
}

variable "enable_dns_hostnames" {
  default = "true"
}
variable "vpc_cidr_block" {
  description = "VPC CIDR BLOCK"
}
variable "public_subnet_1_cidr" {
  description = "public Subnet 1 CIDR"
}
variable "public_subnet_2_cidr" {
  description = "public Subnet 2 CIDR"
}
variable "private_subnet_1_cidr" {
  description = "private Subnet 1 CIDR"
}
variable "private_subnet_2_cidr" {
  description = "private Subnet 2 CIDR"
}
variable "vpc_Name" {
  default = "Deep3-Interview-VPC"
}

variable "vpc_Environment" {}

variable "public_subnet1_availability_zone" {}
variable "public_subnet2_availability_zone" {}
variable "webserver-subnet1_Name" {
  description = "Webserver Public Subnet 1 tag name"
}
variable "webserver-subnet2_Name" {
  description = "Webserver Public Subnet 2 tag name"
}
variable "private_subnet1_availability_zone" {}
variable "private_subnet2_availability_zone" {}

variable "appserver-subnet1_Name" {
  description = "Private Subnet 1 tag name"
}
variable "appserver-subnet2_Name" {
  description = "Private Subnet 2 tag name"
}
variable "private_RT_Name" {
  description = "Private Route Table tag name"
}
variable "public_RT_Name" {
  description = "Public Route Table tag name"
}
variable "log_name" {
  default = "devopslogs"
}

variable "EIP_Name" {
  description = "Elastic IP Address to be use by NAT Gateway"

}

variable "destination_cidr_block" {}
variable "IGW_Name" {}
variable "nat-gw_Name" {}
variable "associate_with_private_ip" {}

########################## ALB #########################



variable "http_port" {}
variable "egress_protocol" {}

variable "alb_internal" {}
variable "load_balancer_type" {}
variable "alb_health_check_interval" {}
variable "alb_health_check_healthy_threshold" {}
variable "alb_health_check_unhealthy_threshold" {}
variable "alb_health_check_timeout" {}
variable "port" {}
variable "alb_listener_webserver_priority" {}
variable "alb_protocol" {}
variable "alb_listener_type" {}

variable "alb_tag_Name" {}
variable "alb_name" {}
variable "alb-targets-group_name" {}
variable "albresource_name" {}