
################### SECURITY GROUP FOR DATABASE #############################################################

resource "aws_security_group" "rds_sg" {
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id
  name        = var.rds_sg_name
  description = "Deep3 DevOps interview RDS security group"

  ingress {
    from_port       = var.rds_port
    to_port         = var.rds_port
    protocol        = var.protocol
    description     = "Allow webserver security group into MYSQL Deep3-Interview database"
    security_groups = [data.terraform_remote_state.ec2.outputs.ec2_sg_id]
  }

  ingress {
    from_port   = var.rds_port
    to_port     = var.rds_port
    protocol    = var.protocol
    description = "MySQL Database Ingress"
    cidr_blocks = [var.cidr_blocks]
  }

  egress {
    from_port   = var.egress_port
    to_port     = var.egress_port
    protocol    = var.egress_protocol
    description = "MySQL Database Egress"
    cidr_blocks = [var.cidr_blocks]
  }
}