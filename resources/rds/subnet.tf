########################### SPECIFY RDS(MYSQL) SUBNET####################################################

resource "aws_db_subnet_group" "mysql-subnet" {
  name = var.rds_subnetgroup_name
  subnet_ids = [data.terraform_remote_state.vpc.outputs.public_subnet_1_id,
  data.terraform_remote_state.vpc.outputs.public_subnet_2_id]

  tags = {
    Name        = var.rds_sg_tag_Name
    Environment = var.vpc_Environment
  }
}

