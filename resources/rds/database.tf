



####################### CREATE AN RDS(MYSQL) DATABASE INSTANCE IN VPC WITH RDS SUBNET AND SECURITY GROUP  ########################################

resource "aws_db_instance" "Deep3-Interview" {
  allocated_storage       = var.allocated_storage
  storage_type            = var.storage_type
  engine                  = var.engine
  engine_version          = var.engine_version
  instance_class          = var.instance_class
  identifier              = var.rds_instance_identifier
  name                    = var.database_name
  username                = var.database_user
  password                = var.database_password
  parameter_group_name    = aws_db_parameter_group.mysql-parameter.id
  db_subnet_group_name    = aws_db_subnet_group.mysql-subnet.id
  multi_az                = var.multi_az
  vpc_security_group_ids  = [aws_security_group.rds_sg.id]
  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window
  skip_final_snapshot     = var.skip_final_snapshot
  apply_immediately       = var.apply_immediately



  tags = {
    Name        = var.rds_instance_tag_Name
    Environment = var.vpc_Environment
  }

}
