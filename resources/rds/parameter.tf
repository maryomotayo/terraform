
######################## DATABASE PARAMETER GROUP #########################################################################

resource "aws_db_parameter_group" "mysql-parameter" {
  name        = var.mysql_name
  family      = var.rds_family
  description = "MysqlDB parameter group"

  parameter {
    name  = var.parameter1_name
    value = var.parameter_value
  }
  parameter {
    name  = var.parameter2_name
    value = var.parameter_value
  }
}