
################Creating an Elastic Load Balancer for webserver#############################

resource "aws_alb" "webapp_load_balancer" {
  name               = var.albresource_name
  internal           = var.alb_internal
  load_balancer_type = var.load_balancer_type
  security_groups    = [aws_security_group.alb_security_group.id]
  subnets = [data.terraform_remote_state.vpc.outputs.public_subnet_1_id,
  data.terraform_remote_state.vpc.outputs.public_subnet_2_id]

  tags = {
    Name        = var.alb_tag_Name
    Environment = var.vpc_Environment
  }
}


########################Creating ALB Target Group###################################


resource "aws_alb_target_group" "webservers" {
  name     = var.alb-targets-group_name
  port     = var.port
  protocol = var.alb_protocol
  vpc_id   = data.terraform_remote_state.vpc.outputs.vpc_id
}


##################Attach webservers to targets group###########################################


resource "aws_alb_target_group_attachment" "webserver-1" {
  target_group_arn = aws_alb_target_group.webservers.arn
  target_id        = data.terraform_remote_state.ec2.outputs.ec2_id[count.index]
  port             = var.port
  count            = 2
}

##############ALB Listener##########################


resource "aws_alb_listener" "webservers" {
  load_balancer_arn = aws_alb.webapp_load_balancer.arn
  port              = var.port
  protocol          = var.alb_protocol

  default_action {
    target_group_arn = aws_alb_target_group.webservers.arn
    type             = var.alb_listener_type

  }

}

#############ALB Listener rule#########################################

resource "aws_alb_listener_rule" "webservers" {
  listener_arn = aws_alb_listener.webservers.arn
  priority     = var.alb_listener_webserver_priority

  action {
    type             = var.alb_listener_type
    target_group_arn = aws_alb_target_group.webservers.arn
  }

  condition {
    path_pattern {
      values = ["/static/*"]
    }
  }

}























