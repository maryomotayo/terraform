
##################Elastic LOAD BALANCER SECURITY GROUP######################################


resource "aws_security_group" "alb_security_group" {
  name        = var.alb_name
  description = "Application load balancer security group"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id

  ingress {
    from_port   = var.egress_port
    to_port     = var.egress_port
    protocol    = var.egress_protocol
    cidr_blocks = [var.cidr_blocks]
    description = "Allow web traffic to load balancer"
  }

  egress {
    from_port   = var.egress_port
    to_port     = var.egress_port
    protocol    = var.egress_protocol
    cidr_blocks = [var.cidr_blocks]
  }
}
