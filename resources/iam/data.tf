data "terraform_remote_state" "ec2" {
  backend = "s3"
  config = {
    region = "eu-west-1"
    key    = "interview/ec2_backend.tfstate"
    bucket = "devops-interview-bucket"
  }
}