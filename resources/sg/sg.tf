
resource "aws_security_group_rule" "elb_to_ec2" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = data.terraform_remote_state.elb.outputs.elb_sg_id
  source_security_group_id = data.terraform_remote_state.ec2.outputs.ec2_sg_id
}

resource "aws_security_group_rule" "ec2_to_elb" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = data.terraform_remote_state.elb.outputs.elb_sg_id
  security_group_id        = data.terraform_remote_state.ec2.outputs.ec2_sg_id
}

